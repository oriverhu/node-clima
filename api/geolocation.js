const axios = require('axios');


const GetData = async(param) => {
    const encodedUrl = 'https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=' + encodeURI(param);

    const instance = axios.create({
        //Mi Work
        // baseURL: 'https://devru-latitude-longitude-find-v1.p.rapidapi.com/latlon.php?location=' + argv.direccion.replace(' ', '+'),
        baseURL: encodedUrl,
        headers: { 'X-Rapidapi-Key': '263e2b5f0bmshd7507b0fcc40ae2p1299bdjsn19fd10214729' }
    });

    // instance.get()
    //     .then(r => {
    //         console.log(r.data.Results[0])
    //     })
    //     .catch(e => console.log(String(e)))

    const resp = await instance.get();
    if (resp.data.Results.length === 0) {
        throw new Error(`No hay resultados para ${param}`)
    }
    const data = resp.data.Results[0];
    return {
        direccion: data.name,
        lat: data.lat,
        long: data.lon,
    }
}

module.exports = {
    GetData
}