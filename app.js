const argv = require("yargs").options({
    direccion: {
        alias: 'd',
        desc: 'Dirección de la ciudad para obtener el clima',
        demand: true
    }
}).argv;
const { GetData } = require('./api/geolocation');
const { GetClima } = require('./api/clima');


// GetData(argv.direccion)
//     .then(respuesta => {
//         GetClima(respuesta.lat, respuesta.long).then(r => {
//             console.log(`El clima de ${respuesta.direccion} es de ${r}`)
//         }).catch(e => console.log(String(e)))
//     })
//     .catch(e => console.log(String(e)))

//esto es lo mismo que arriba (funcion comentada)
const getInfo = async(direccion) => {
    const coords = await GetData(direccion);
    const temp = await GetClima(coords.lat, coords.long);

    try {
        console.log(`El clima de ${direccion} es de ${temp}`);
    } catch (e) {
        console.log(String(e));
    }
}

getInfo(argv.direccion)